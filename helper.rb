# frozen_string_literal: true

require 'gitlab'

module Helper
  def active?(project_id, issue_iid, days)
    token = network.options.token || ''

    @project_id = project_id
    @issue_iid = issue_iid
    Gitlab.endpoint = ENV.fetch('CI_API_V4_URL')
    Gitlab.private_token = token
    @bot_username ||= token != '' ? Gitlab.user.username : ''

    days_since_last_human_update <= days
  end

  def age_of_timestamp(timestamp, from = Date.today)
    (from - Date.parse(timestamp)).to_i
  end

  def days_since_last_human_update
    notes = Gitlab.issue_notes(@project_id, @issue_iid, { order_by: "updated_at", sort: "desc" })
    notes.each do |note|
      next if note['author']['username'] == @bot_username

      return age_of_timestamp(note['updated_at'])
    end

    9999 # No human updates found (should never happen)
  end
end

Gitlab::Triage::Resource::Context.include Helper
