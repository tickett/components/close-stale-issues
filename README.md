# Close Stale Issues

Close Stale Issues will:

- Add a `stale` label on issues after 90 days of inactivity.
- Close issues with the `stale` label after 14 further days of inactivity.
- An update/comment will trigger the `stale` label to be removed.

## Usage

```yml
include:
  - component: gitlab.com/tickett/components/close-stale-issues/close-stale-issues@main
    inputs:
      api_token: $REPORTER_API_TOKEN
      days_until_stale: 60
      days_until_close: 7
      dry_run: true
      group_id: '9970'
      job_suffix: ':dry-run'
      label: inactive
```

## Inputs

### `api_token`

GitLab API token.

The API token needs the `api` scope to be able to retreive and update issues.

### `closed_message`

Message to announce when an issue is closed.

To enter a multi-line message, use the following format:

```yml
closed_message: >
  This is a message.
  This will render on the same line.\n\n
  This will render a new paragraph.
```

* Terminate a line with `\n\n` to start a new paragraph.
* Do not leave any lines blank.

### `days_until_close`

Number of days after labelling as stale before closing issues.

### `days_until_stale`

Number of days inactivity before labelling issues as stale.

### `debug`

Print debug information.

### `dry_run`

Perform a dry run.

An `api_token` with only `read_api` scope can be used when performing a dry run.

### `enable`

Enable the job.

This allows configuring the job not to run in certain scenarios.

### `group_id`

Group id or path to triage.

When using an id, but sure to enclose it in quotes.

### `host_url`

The base URL of the GitLab instance (including protocol and port).

### `job_suffix`

A suffix to add to the job name.

This is only needed if you are including this component multiple times.

### `label`

The label used to identify stale issues.

### `project_id`

Project id or path to triage.

When using an id, but sure to enclose it in quotes.

### `stage`

The stage in which to run the job.

### `stale_message`

Message to announce when an issue is marked as stale.

To enter a multi-line message, use the following format:

```yml
closed_message: >
  This is a message.
  This will render on the same line.\n\n
  This will render a new paragraph.
```

* Terminate a line with `\n\n` to start a new paragraph.
* Do not leave any lines blank.
